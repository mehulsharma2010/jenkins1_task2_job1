#!/bin/sh
for pkg; do
    dpkg -s "$pkg" >/dev/null 2>&1 && {
        echo "$pkg is installed."
    } || {
        sudo apt install $pkg
    }
done
